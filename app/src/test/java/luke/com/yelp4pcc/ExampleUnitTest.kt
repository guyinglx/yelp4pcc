package luke.com.yelp4pcc

import luke.com.yelp4pcc.utils.Utils
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun testUtilsformatDate(){
        assertEquals("", Utils.formatDate(""))
        assertEquals("", Utils.formatDate(null))
        assertEquals("2018-05-19", Utils.formatDate("2018-05-19"))
        assertEquals("May 19, 2018", Utils.formatDate("2018-05-19 21:23:23"))
    }
}
