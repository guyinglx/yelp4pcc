package luke.com.yelp4pcc.utils

/**
 * Created by lukelin on 2018-05-24.
 */
object Extras {
    val DATA = "data"
    val LAT = "lat"
    val LON = "lon"
}
