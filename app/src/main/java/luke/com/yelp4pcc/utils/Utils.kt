package luke.com.yelp4pcc.utils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import luke.com.yelp4pcc.R
import luke.com.yelp4pcc.pojos.Coordinate
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by lukelin on 2018-05-24.
 */
class Utils {
    companion object {
        fun showDialog(context: Context, message: String, title: String, onClickListener: DialogInterface.OnClickListener) {
            val builder = AlertDialog.Builder(context)
            builder.setMessage(message)
                    .setTitle(title)
                    .setCancelable(true)
                    .setNegativeButton(R.string.cancel, null)
                    .setPositiveButton(R.string.ok, onClickListener)
            val alert = builder.create()
            alert.show()
        }

        fun switchFragment(fragmentManager: FragmentManager, fragment: Fragment, fragmentContainerId: Int,
                           tag: String? = null) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(fragmentContainerId, fragment, tag)
            fragmentTransaction.commitAllowingStateLoss()
        }

        fun getLocation(sharedPref: SharedPrefPersistence): Coordinate {
            val lat = sharedPref.read(Extras.LAT) ?: "45.5576979"
            val lon = sharedPref.read(Extras.LON) ?: "-74.0111753"
            return Coordinate(lat.toDouble(), lon.toDouble())
        }

        fun saveLocation(sharedPref: SharedPrefPersistence, coordinate: Coordinate) {
            sharedPref.save(Extras.LAT, coordinate.latitude.toString())
            sharedPref.save(Extras.LON, coordinate.longitude.toString())
        }

        fun formatDate(time_created: String?): String {
            return try {
                val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(time_created)
                SimpleDateFormat("MMM dd, yyyy", Locale.US).format(date)
            } catch (e: Exception) {
                time_created?:""
            }
        }
    }
}
