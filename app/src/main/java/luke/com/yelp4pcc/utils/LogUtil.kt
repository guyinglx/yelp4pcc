package luke.com.yelp4pcc.utils

import android.util.Log

/**
 * Created by lukelin on 2018-05-24.
 */
object LogUtil {
    fun log(info: String, tag: String = "Luke"){
        Log.d(tag, info)
    }
}