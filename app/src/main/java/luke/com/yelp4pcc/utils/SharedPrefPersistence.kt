package luke.com.yelp4pcc.utils

import android.content.Context
import android.content.SharedPreferences

class SharedPrefPersistence(context: Context) {
    private var prefs : SharedPreferences? = null

    init {
        prefs = context.getSharedPreferences("SharedPrefPersistence", Context.MODE_PRIVATE)
    }

    fun save(key: String, value: String?){
        prefs?.edit()?.putString(key, value)?.apply()
    }

    fun read(key: String): String?{
        return prefs?.getString(key, null)
    }

    fun removeKey(key: String){
        prefs?.edit()?.remove(key)?.apply()
    }
}