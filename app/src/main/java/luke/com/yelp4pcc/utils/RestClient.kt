package luke.com.yelp4pcc.utils

import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import luke.com.yelp4pcc.pojos.BusinessFullDetail
import luke.com.yelp4pcc.pojos.BusinessListReturn
import luke.com.yelp4pcc.pojos.BusinessReviewReturn
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by lukelin on 2018-05-24.
 */
object RestClient {

    private const val URL_BASE = "https://api.yelp.com"
    private val httpClient = OkHttpClient.Builder()
    private val builder = Retrofit.Builder()
            .baseUrl(URL_BASE)
            .addConverterFactory(GsonConverterFactory.create())
    private const val apiKey = "zhsnN0cs2STFmvIaFnzCvm2PdYl_PuwhQu9fMV4DpdM_1IfItaiHBcMEIgHRzIS2PnXvbOYR5BhwhWhjmek0PAu9n72B4QxrYqf8pXu-BLyD9sz6l5AGK_0jdBVZWnYx"

    private fun <S> createService(serviceClass: Class<S>): S {
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                    .header("Authorization", "Bearer $apiKey")
                    .header("Accept", "application/json")
                    .method(original.method(), original.body())
            val request = requestBuilder.build()
            chain.proceed(request)
        }
        val client = httpClient.build()
        val retrofit = builder.client(client).build()
        return retrofit.create(serviceClass)
    }

    interface RestfulService {

        @GET("/v3/businesses/search")
        fun getBusinessList(@Query("term") term: String, @Query("latitude") latitude: Double, @Query("longitude") longitude: Double, @Query("limit") limit: Int = 30): Call<BusinessListReturn>

        @GET("/v3/businesses/{id}")
        fun getBusinessDetail(@Path("id") id: String): Call<BusinessFullDetail>

        @GET("/v3/businesses/{id}/reviews")
        fun getBusinessReviews(@Path("id") id: String): Call<BusinessReviewReturn>

    }

    var restfulService = createService(RestfulService::class.java)

    fun getBusinessDetailInfo(id: String) : Observable<Pair<BusinessFullDetail, BusinessReviewReturn>>{
        val detail = Observable.create<BusinessFullDetail> { subscriber ->
            val call = RestClient.restfulService.getBusinessDetail(id)
            call.enqueue(object : Callback<BusinessFullDetail> {
                override fun onResponse(call: Call<BusinessFullDetail>, response: Response<BusinessFullDetail>) {
                    if(response.body()!=null){
                        subscriber.onNext(response.body()!!)
                        subscriber.onComplete()
                    }
                }

                override fun onFailure(call: Call<BusinessFullDetail>, t: Throwable) {
                    subscriber.onError(t)
                }
            })
        }
        val reviews = Observable.create<BusinessReviewReturn> { subscriber ->
            val call = RestClient.restfulService.getBusinessReviews(id)
            call.enqueue(object : Callback<BusinessReviewReturn> {
                override fun onResponse(call: Call<BusinessReviewReturn>, response: Response<BusinessReviewReturn>) {
                    if(response.body()!=null){
                        subscriber.onNext(response.body()!!)
                        subscriber.onComplete()
                    }
                }

                override fun onFailure(call: Call<BusinessReviewReturn>, t: Throwable) {
                    subscriber.onError(t)
                }
            })
        }

        return Observable.zip(detail, reviews, BiFunction<BusinessFullDetail, BusinessReviewReturn, Pair<BusinessFullDetail, BusinessReviewReturn>> { t1, t2 ->
            Pair(t1, t2)
        })
    }
}