package luke.com.yelp4pcc.ui

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

import android.support.v4.view.ViewPager
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import luke.com.yelp4pcc.R
import luke.com.yelp4pcc.enums.TabCategory
import luke.com.yelp4pcc.utils.Extras
import java.util.*

/**
 * Created by lukelin on 2018-05-24.
 */
class MainActivity : LocationBaseActivity() {
    override fun getLayoutRes() = R.layout.activity_main

    override fun fetchData() {
        super.fetchData()
        setupViewPager()
        val tabLayout = tabs
        tabLayout.setupWithViewPager(viewPager)
    }

    private var viewPager: ViewPager? = null
    private var adapter: SimpleAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val toolbar = toolbar
        setSupportActionBar(toolbar)
        viewPager = viewpager
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search -> startActivity(Intent(this@MainActivity, SearchActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupViewPager() {
        adapter = SimpleAdapter(supportFragmentManager)
        for (category in TabCategory.values()) {
            adapter?.addFragment(category, resources)
        }
        viewPager?.adapter = adapter
    }

    internal inner class SimpleAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        private val mFragments = ArrayList<BusinessListFragment>()
        private val mFragmentTitles = ArrayList<String>()

        fun addFragment(tabCategory: TabCategory, res: Resources) {
            val fragment = BusinessListFragment()
            val bundle = Bundle()
            bundle.putString(Extras.DATA, tabCategory.name)
            fragment.arguments = bundle
            mFragments.add(fragment)
            mFragmentTitles.add(res.getString(tabCategory.titleResId))
        }

        override fun getItem(position: Int): Fragment {
            return mFragments[position]
        }

        override fun getCount(): Int {
            return mFragments.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitles[position]
        }
    }

}
