package luke.com.yelp4pcc.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_business_list.view.*
import kotlinx.android.synthetic.main.list_item.view.*
import luke.com.yelp4pcc.R
import luke.com.yelp4pcc.pojos.BusinessListReturn
import luke.com.yelp4pcc.pojos.BusinessSimpleDetail
import luke.com.yelp4pcc.utils.Extras
import luke.com.yelp4pcc.utils.RestClient
import luke.com.yelp4pcc.utils.SharedPrefPersistence
import luke.com.yelp4pcc.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by lukelin on 2018-05-24.
 */
/**
 * This fragment is used to show business list.
 */
class BusinessListFragment : ClickToRefreshFragmentBase() {
    private var recyclerView: RecyclerView? = null
    private var term: String? = null
    private lateinit var sharedPrefPersistence: SharedPrefPersistence

    override val layoutId: Int
        get() = R.layout.fragment_business_list

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        term = arguments?.getString(Extras.DATA)
        sharedPrefPersistence = SharedPrefPersistence(activity as Context)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun initView(spView: View) {
        recyclerView = spView.recyclerview
        recyclerView?.layoutManager = GridLayoutManager(activity, 2)
    }

    override fun doRefresh(): Observable<Any> {
        return Observable.create { subscriber ->
            val coordinate = Utils.getLocation(sharedPrefPersistence)
            val call = RestClient.restfulService.getBusinessList(term?:"", coordinate.latitude!!, coordinate.longitude!!)
            call.enqueue(object : Callback<BusinessListReturn> {
                override fun onResponse(call: Call<BusinessListReturn>, response: Response<BusinessListReturn>) {
                    if(response.body()!=null){
                        subscriber.onNext(response.body()!!)
                    }
                }

                override fun onFailure(call: Call<BusinessListReturn>, t: Throwable) {
                    subscriber.onError(t)
                }
            })
        }
    }

    override fun refreshUI(mainContent: RelativeLayout?, passData: Any) {
        if (passData !is BusinessListReturn) return
        if(activity != null){
            recyclerView?.adapter = SimpleRecyclerViewAdapter(passData.businesses?.sortedBy { it.name }?:ArrayList())
        }
    }

    class SimpleRecyclerViewAdapter(private val list: List<BusinessSimpleDetail>) : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item, parent, false)
            val viewHolder = ViewHolder(view)
            viewHolder.itemView.setOnClickListener{
                val context = it.context
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra(Extras.DATA, list[viewHolder.adapterPosition].id)
                context.startActivity(intent)
            }
            return viewHolder
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val businessSimpleDetail = list[position]
            holder.name.text = businessSimpleDetail.name
            holder.reviewCount.text = holder.itemView.context.getString(R.string.business_view_holder_count, businessSimpleDetail.review_count)
            holder.ratingBar.rating = businessSimpleDetail.rating?.toFloat()?:0f
            Glide.with(holder.image.context)
                    .load(businessSimpleDetail.image_url)
                    .fitCenter()
                    .into(holder.image)
        }

        override fun getItemCount(): Int {
            return list.size
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.list_item_avatar
        val name: TextView = view.list_item_name
        val reviewCount: TextView = view.list_item_review_count
        val ratingBar: RatingBar = view.list_item_rating
    }
}
