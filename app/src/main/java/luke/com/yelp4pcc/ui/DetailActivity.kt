package luke.com.yelp4pcc.ui

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.activity_detail_reviews_viewholder.view.*
import kotlinx.android.synthetic.main.card_layout.view.*
import luke.com.yelp4pcc.R
import luke.com.yelp4pcc.R.id.*
import luke.com.yelp4pcc.pojos.BusinessFullDetail
import luke.com.yelp4pcc.pojos.BusinessReviewReturn
import luke.com.yelp4pcc.pojos.Review
import luke.com.yelp4pcc.utils.Extras
import luke.com.yelp4pcc.utils.RestClient
import luke.com.yelp4pcc.utils.Utils

/**
 * Created by lukelin on 2018-05-24.
 */
class DetailActivity : AppCompatActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val intent = intent
        val id = intent.getStringExtra(Extras.DATA)
        val toolbar = toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val collapsingToolbar = collapsing_toolbar
        val businessCall = RestClient.getBusinessDetailInfo(id)
        businessCall.subscribe({
            setupView(it.first, it.second)
            collapsingToolbar.title = it.first.name
        }, {

        })
    }

    private fun setupView(business: BusinessFullDetail?, reviewReturn: BusinessReviewReturn?) {
        val imageView = backdrop
        val address = activity_detail_address
        val tel = activity_detail_tel
        val review = activity_detail_review
        val titleTextView = card_layout_title
        val list = card_layout_list
        val ratingBar = card_layout_rating

        Glide.with(this).load(business?.largeImageUrl()).centerCrop().into(imageView)
        setupCardView(address, getString(R.string.detail_activity_address), business?.location?.getDisplayAddress()?:"")
        setupCardView(tel, getString(R.string.detail_activity_phone), business?.phone?:"")

        if (reviewReturn?.reviews?.isNotEmpty() == true) {
            review.visibility = View.VISIBLE
            ratingBar.rating = business?.rating?.toFloat()?:0f
            titleTextView.text = getString(R.string.detail_activity_latest_review, reviewReturn.reviews.size, reviewReturn.total)
            list.layoutManager = LinearLayoutManager(this)
            list.adapter = ReviewAdapter(this, reviewReturn.reviews)
        }

        address.setOnClickListener {
            val displayAddress = business?.location?.getDisplayAddress()
            if(displayAddress != null){
                Utils.showDialog(this@DetailActivity, getString(R.string.you_will_be_guided_to, displayAddress), getString(R.string.open_google_map), DialogInterface.OnClickListener { _, _ ->
                    val gmmIntentUri = Uri.parse("geo:0,0?q=$displayAddress")
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    mapIntent.`package` = "com.google.android.apps.maps"
                    this@DetailActivity.startActivity(mapIntent)
                })
            }
        }
        tel.setOnClickListener {
            Utils.showDialog(this@DetailActivity, getString(R.string.you_will_be_calling, business?.phone), getString(R.string.make_phone_call), DialogInterface.OnClickListener { _, _ ->
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + business?.phone))
                this@DetailActivity.startActivity(intent)
            })
        }
        review.setOnClickListener {
            Utils.showDialog(this@DetailActivity, getString(R.string.we_got_reviews_want_add_more, business?.review_count), getString(R.string.go_to_web), DialogInterface.OnClickListener { _, _ ->
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(business?.url?:""))
                startActivity(browserIntent)
            })
        }
    }

    private fun setupCardView(view: View, title: String, content: String) {
        val titleTextView = view.card_layout_title
        val contentTextView = view.card_layout_content
        titleTextView.text = title
        contentTextView.text = content
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) finish()
        return super.onOptionsItemSelected(item)
    }

    class ReviewAdapter(private val context: Context, private val list: List<Review>) : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.activity_detail_reviews_viewholder, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val review = list[position]
            holder.contentTextView.text = review.text
            holder.name.text = review.user?.name
            holder.time.text = Utils.formatDate(review.time_created)
            Glide.with(context).load(review.user?.image_url?:"").fitCenter().into(holder.userImage)
        }

        override fun getItemCount(): Int {
            return list.size
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val contentTextView: TextView = view.review_content
        val name: TextView = view.review_user_name
        val time: TextView = view.review_user_create_time
        val userImage: ImageView = view.review_user_image
    }

}
