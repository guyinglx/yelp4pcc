package luke.com.yelp4pcc.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_search.*
import luke.com.yelp4pcc.utils.Extras
import luke.com.yelp4pcc.R
import luke.com.yelp4pcc.utils.Utils

/**
 * Created by lukelin on 2018-05-24.
 */
class SearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        with(activity_search_search_view){
            setOnQueryTextListener(MySearchViewListener())
            isIconified = false
            queryHint = getString(R.string.search_hint)
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private inner class MySearchViewListener : android.support.v7.widget.SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(searchText: String): Boolean {
            return submitTextChanges(searchText)
        }

        override fun onQueryTextChange(searchText: String): Boolean {
            return true
        }
    }

    private fun submitTextChanges(searchText: String): Boolean {
        //when the query submit, we create a new BusinessListFragment with query.
        val fragment = BusinessListFragment()
        val bundle = Bundle()
        bundle.putString(Extras.DATA, searchText)
        fragment.arguments = bundle
        Utils.switchFragment(supportFragmentManager, fragment, R.id.fragment_container)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) finish()
        return super.onOptionsItemSelected(item)
    }

}
