package luke.com.yelp4pcc.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.click_to_refresh_fragment_base.view.*
import luke.com.yelp4pcc.R

/**
 * Created by lukelin on 2018-05-24.
 */
abstract class ClickToRefreshFragmentBase : Fragment() {

    private var mainContent: RelativeLayout? = null
    private var refreshButton: TextView? = null
    private var progressBar: ProgressBar? = null
    private var subscription: Disposable? = null

    protected abstract val layoutId: Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.click_to_refresh_fragment_base, container, false)
        val spView = inflater.inflate(layoutId, container, false)
        mainContent = view.click_to_refresh_fragment_maincontent
        mainContent?.addView(spView)
        initView(spView)
        refreshButton = view.click_to_refresh_fragment_refresh_button
        progressBar = view.click_to_refresh_fragment_progress
        refreshButton?.setOnClickListener { refresh() }
        refresh()
        return view
    }

    protected abstract fun initView(spView: View)

    private fun refresh() {
        subscription?.dispose()
        this.subscription = doRefresh()
                .doOnSubscribe { onProgress() }
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onSuccess(it)
                }, {
                    onFail()
                })
    }

    protected abstract fun doRefresh(): Observable<Any>

    private fun onFail() {
        mainContent?.visibility = View.INVISIBLE
        refreshButton?.visibility = View.VISIBLE
        progressBar?.visibility = View.INVISIBLE
    }

    private fun onSuccess(`object`: Any) {
        mainContent?.visibility = View.VISIBLE
        refreshButton?.visibility = View.INVISIBLE
        progressBar?.visibility = View.INVISIBLE
        refreshUI(mainContent, `object`)
    }

    protected abstract fun refreshUI(mainContent: RelativeLayout?, `object`: Any)

    private fun onProgress() {
        mainContent?.visibility = View.INVISIBLE
        refreshButton?.visibility = View.INVISIBLE
        progressBar?.visibility = View.VISIBLE
    }


    override fun onDestroy() {
        subscription?.dispose()
        super.onDestroy()
    }

}
