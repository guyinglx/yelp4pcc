package luke.com.yelp4pcc.pojos

/**
 * Created by lukelin on 2018-05-24.
 */
class BusinessFullDetail : BusinessBaseDetail(){
    val hours: List<Hour>? = null
    val is_claimed: Boolean? = null
    val photos: List<String>? = null
}