package luke.com.yelp4pcc.pojos

/**
 * Created by lukelin on 2018-05-24.
 */
data class User(
    val name : String? = null,
    val image_url : String? = null
)