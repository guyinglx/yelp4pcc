package luke.com.yelp4pcc.pojos

/**
 * Created by lukelin on 2018-05-24.
 */
data class BusinessListReturn(
        val total : Int? = null,
        var businesses: List<BusinessSimpleDetail>? = null,
        var region: Region? = null
)