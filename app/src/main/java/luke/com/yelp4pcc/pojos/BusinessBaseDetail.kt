package luke.com.yelp4pcc.pojos

/**
 * Created by lukelin on 2018-05-24.
 */
open class BusinessBaseDetail(
    val categories : List<Category>? = null,
    val coordinates : Coordinate? = null,
    val display_phone: String? = null,
    val id: String? = null,
    val alias: String? = null,
    val image_url: String? = null,
    val is_closed	: Boolean? = null,
    val location: Location? = null,
    val name: String? = null,
    val phone: String? = null,
    val price: String? = null,
    val rating: Double? = null,
    val review_count: Int? = null,
    val url: String? = null,
    val transactions: List<String>? = null

){
    fun largeImageUrl() = image_url?.replace("o.jpg", "l.jpg")
}