package luke.com.yelp4pcc.pojos

/**
 * Created by lukelin on 2018-05-24.
 */
data class Open(
    val day : Int? = null,
    val start : String? = null,
    val end : String? = null,
    val is_overnight : String? = null
)