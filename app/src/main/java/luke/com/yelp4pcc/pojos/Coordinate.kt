package luke.com.yelp4pcc.pojos

/**
 * Created by lukelin on 2018-05-24.
 */
data class Coordinate(
    val latitude : Double? = null,
    val longitude : Double? = null
)