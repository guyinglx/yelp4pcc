package luke.com.yelp4pcc.pojos

/**
 * Created by lukelin on 2018-05-24.
 */
data class Location(
    val address1 : String? = null,
    val address2 : String? = null,
    val address3 : String? = null,
    val city : String? = null,
    val country : String? = null,
    val cross_streets : String? = null,
    val display_address : List<String>? = null,
    val state : String? = null,
    val zip_code : String? = null
){
    fun getDisplayAddress() = display_address?.joinToString(", ")
}