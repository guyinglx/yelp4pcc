package luke.com.yelp4pcc.pojos

/**
 * Created by lukelin on 2018-05-24.
 */
data class Hour(
    val is_open_now : Boolean? = null,
    val hours_type : String? = null,
    val open : List<Open>? = null
)