package luke.com.yelp4pcc.pojos

/**
 * Created by lukelin on 2018-05-24.
 */
data class Review(
    val id : String? = null,
    val text : String? = null,
    val url : String? = null,
    val rating : Int? = null,
    val time_created : String? = null,
    val user : User? = null
)