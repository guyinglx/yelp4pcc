package luke.com.yelp4pcc.pojos

/**
 * Created by lukelin on 2018-05-24.
 */
data class Category(
    val alias : String? = null,
    val title : String? = null
)