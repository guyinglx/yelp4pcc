package luke.com.yelp4pcc.pojos

/**
 * Created by lukelin on 2018-05-24.
 */
data class BusinessReviewReturn(
    val total : Int? = null,
    val possible_languages: List<String>? = null,
    val reviews: List<Review>? = null
)