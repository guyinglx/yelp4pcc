package luke.com.yelp4pcc.enums

import luke.com.yelp4pcc.R

/**
 * Created by lukelin on 2018-05-24.
 */
enum class TabCategory(val titleResId: Int) {
    Chinese(R.string.category_chinese),
    Sushi(R.string.category_sushi),
    Ramen(R.string.category_ramen),
    Koran(R.string.category_korean)
}
